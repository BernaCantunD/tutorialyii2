<?php
/**
 * Created by PhpStorm.
 * User: Berna
 * Date: 14/09/2017
 * Time: 13:24
 */
namespace app\models;
use Yii;
use yii\db\ActiveRecord;

class Alumnos extends ActiveRecord
{

    public static function getDb()
    {
        return Yii::$app->db;

    }
    public static function tableName()
    {
        return 'alumnos';
    }
}